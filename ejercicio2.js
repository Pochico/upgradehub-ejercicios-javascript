function ruedasDeJuguete(diamRueda) {
    if (diamRueda <= 10) {
        console.log('La rueda es para un juguete pequeño');
    }
    else if (diamRueda > 10 && diamRueda < 20) {
        console.log('La rueda es para un juguete mediano');
    }
    else if (diamRueda >= 20) {
        console.log('La rueda es para un juguete grande');
    }
}

ruedasDeJuguete(6);
ruedasDeJuguete(15);
ruedasDeJuguete(24);