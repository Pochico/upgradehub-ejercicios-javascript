var textoInicial = 'Follow that bus! Sigue a ese arbusto!';
var textoFinal = '';

for (i = 0; i < textoInicial.length; i++) {
    textoFinal +=
        textoInicial.charAt(i) +
        (i == textoInicial.length - 1 || textoInicial.charAt(i) == ' '
            ? ''
            : '-');
}

console.log(textoFinal);