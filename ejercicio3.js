var arrLetras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'];

// Versión con dos prompt separados en funciones

// var numDni = prompt("Por favor, escriba su DNI sin letra");
// numCorrecto(numDni);
// var letDni =  prompt("Por favor, escriba la letra de su DNI");


// validateDni(numDni, letDni);

// function numCorrecto(numero) {
//     if (numero < 0 || numero > 99999999) {
//         prompt("El número proporcionado no es correcto, inténtelo de nuevo");
//     }
// }

// function validateDni(numero, letra) {
//     letra == arrLetras[numero % 23]
//         ? alert("DNI Válido")
//         : alert(
//             "DNI Incorrecto, por favor, recargue la página e inténtelo de nuevo"
//         );
//     console.log(letra);
// }




// Versión con un sólo prompt

var fullDni = prompt('Por favor, introduzca su DNI con letra');


validateFullDni(fullDni);

function validateFullDni(fullDni) {
    console.log(fullDni);
    numDni = fullDni.substr(0, fullDni.length - 1);
    letra = fullDni.substr(fullDni.length - 1).toUpperCase();
    console.log(numDni);
    console.log(letra);
    letra == arrLetras[numDni % 23]
        ? alert("DNI Válido")
        : alert(
            "DNI Incorrecto, por favor, recargue la página e inténtelo de nuevo"
        );
}



// Versión sólo para consola

// const readline = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout
// });

// readline.question('Por favor, introduzca su DNI con letra ', fullDni => {
//     validateFullDni(fullDni);
//     readline.close();
// });

// function validateFullDni(fullDni) {
//     console.log(`Su dni es:  ${fullDni}`);
//     numDni = fullDni.substr(0, fullDni.length - 1);
//     letra = fullDni.substr(fullDni.length - 1).toUpperCase();
//     console.log(numDni);
//     console.log(letra);
//     letra == arrLetras[numDni % 23]
//         ? console.log("DNI Válido")
//         : console.log(
//             "DNI Incorrecto, por favor, recargue la página e inténtelo de nuevo"
//         );
// }
