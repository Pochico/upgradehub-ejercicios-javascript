function miFuncionDeComparar(num1, num2) {
    if (num1 === 5) {
        console.log('num1 es estrictamente igual a 5');
    }
    if (num1 <= num2) {
        console.log('num1 no es mayor que num2');
    }
    if (num2 > 0) {
        console.log('num2 es positivo');
    }
    if (num1 < 0) {
        console.log('num1 es negativo o distinto a 0')
    }
}

miFuncionDeComparar(5, 2);